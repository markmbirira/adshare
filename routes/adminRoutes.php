<?php

$app->group("/admin", function() use ($app) {
    
    /**
     *  admin dashboard:
     *  Displays Admin users
     */
    $app->get('', function($request, $response, $arguments) 
    {       
        $User = new User($this->db);
        if ($User->isLoggedIn()) {
            // $this->flash->addMessage('success', 'Welcome admin');

            // return header('Location:/auth/login');
            // return $response->withRedirect('/auth/login', 302);
        }

        // $user_id = (int)$_SESSION['id'];
        // if ($User->isAdmin($user_id) == true) {
        //     $this->flash->addMessage('error', 'You must be admin to access');

        //     return $response->withRedirect('/auth/login', 302);
        // }

        $article = new Advertisement($this->db);
        $data = $article->listPosts(1);

        $user_id = (int)$_SESSION['id'];

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $Notification = new Notification($this->db);
        $notifications = $Notification->hasNotifications($user_id);

        $messages = $this->flash->getMessages();

        $context = array(
            'data' => $data,
            'categories' => $categories ,
            'notifications' => $notifications,
            'messages' => $messages
        );

        return $this->view->render($response, 'admin/index.html', $context);
        // echo json_encode($data);
    });

    /**
     * advert list
     */
    $app->get('/advert', function($request, $response, $arguments) 
    {
        $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $article = new Advertisement($this->db);
        $data = $article->listPosts(1);

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $Notification = new Notification($this->db);
        $notifications = $Notification->hasNotifications($user_id);

        $messages = $this->flash->getMessages();

        $context = array(
            'data' => $data,
            'categories' => $categories ,
            'notifications' => $notifications,
            'messages' => $messages
        );

        return $this->view->render($response, 'admin/adverts.html', $context);
        // echo json_encode($data);
    });

    /**
     * advert update
     */
    $app->post('/advert/update', function ($request, $response)
    {
    //     $User = new User($this->db);
    //     if (!$User->isLoggedIn($_SESSION)) {
    //         $this->flash->addMessage('error', 'You must be logged in to access');
            
    //         return $response->withRedirect('/auth/login', 302);
    //     }

    //     $user_id = (int)$_SESSION['id'];
    //     if (!$User->isAdmin($user_id)) {
    //         $this->flash->addMessage('error', 'You must be admin to access');

    //         return $response->withRedirect('/auth/login', 302);
    //     }

        $Advertisement = new Advertisement($this->db);

        $data = $request->getParsedBody();
        $files = $request->getUploadedFiles();
      
        #uploaded file
        // $file = $files['path'];

        // if ($file->getError() === UPLOAD_ERR_OK or NULL) {
        //    $file_name = $file->getClientFilename();
        //    $file_type = $file->getClientMediaType();
        //    $path = "/uploads/$file_name";
        //    $file->moveTo("uploads/$file_name");
        // } else {
        //    $file_type = null;
        //    $path = null;
        // }
  
        $post_id = $data['post_id'];
        $advert = $Advertisement->advertDetail($post_id);
      
        $user_id = $advert[0]['id'];

        $title = filter_var($data['title'], FILTER_SANITIZE_STRING);
        $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
        $category_id = $data['category_id'];
        $created_time = date('Y:m:d H:i:s');
        $status = 1;
        $votes = 0;
  
        $Advertisement->updatePost($post_id, $title, $description, $category_id, $user_id, $created_time, $status, $votes);

        # redirect after inserting
        $this->flash->addMessage('success', 'Advert updated successfully');
        return $response->withRedirect('/admin/advert', 302);
    });

    /**
     * advert delete
     */
    $app->post('/advert/delete', function ($request, $response)
    {   
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
           
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $Advertisement = new Advertisement($this->db);

        $data = $request->getParsedBody();
        $files = $request->getUploadedFiles();
      
        $post_id = $data['advert_id'];
  
        $Advertisement->delete(0, $post_id);
        $this->flash->addMessage('success', 'Success, advert deleted');

        # redirect after inserting
        $this->flash->addMessage('Success', 'Advert deleted successfully');
        return $response->withRedirect('/admin/advert', 302);
    });

    /**
     * all categories
     */
    $app->get('/categories', function($request, $response, $arguments) 
    {   
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
        //     # TODO: redirect to admin login page
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $Category = new Category($this->db);
        $data = $Category->getCategories();

        $messages = $this->flash->getMessages();

        $context = array(
            'data' => $data,
            'messages' => $messages
        );

        return $this->view->render($response, 'admin/categories.html', $context);
        // echo json_encode($data);
    });

    /**
     * create category
     */
    $app->post('/categories', function($request, $response) 
    {   
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
           
        //     return $response->withRedirect('/auth/login', 302);
        // }
        
        $Category = new Category($this->db);

        $data = $request->getParsedBody();

        $category_id = $data['category_id'];
        $name = $data['name'];
        $description = $data['description'];
        $created_time = date('Y:m:d H:i:s');

        $Category->setCategory($name, $description, $created_time);

        $this->flash->addMessage('success', 'Category created successfully');

        return $response->withRedirect('/admin/categories', 302);
        // echo json_encode($data);
    });

    /**
     * update category
     */
    $app->post('/categories/update', function($request, $response) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
           
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }
        
        $Category = new Category($this->db);

        $data = $request->getParsedBody();

        $category_id = $data['category_id'];
        $name = $data['name'];
        $description = $data['description'];

        $Category->updateCategory($category_id, $name, $description);

        $this->flash->addMessage('success', 'Category successfully');

        return $response->withRedirect('/admin/categories', 302);
        // echo json_encode($data);
    });

     /**
     * delete category
     */
    $app->post('/categories/delete', function($request, $response) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
           
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $Category = new Category($this->db);
        $data = $request->getParsedBody();
        $category_id = $data['category_id'];

        $Category->deleteCategory($category_id, 0);

        $this->flash->addMessage('success', 'Category deleted successfully');
        
        return $response->withRedirect('/admin/categories', 302);
        // echo json_encode($data);
    });

    /**
     * list all users
     */
    $app->get('/users', function($request, $response, $arguments) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
        //     # TODO: redirect to admin login page
        //     header('Location:/auth/login');
        // }

        $User = new User($this->db);

        $data = $User->getUsers();
        $messages = $this->flash->getMessages();

        $context = array(
            'data' => $data,
            'messages' => $messages
        );

        return $this->view->render($response, 'admin/users.html', $context);
        // echo json_encode($data);
    });

    /**
     * create user
     */
    $app->post('/users', function($request, $response)
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $user = new User($this->db);
        $data = $request->getParsedBody();

        $username = $data['username'];
        $email = $data['email'];
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $password = $data['password'];
        $password2 = $data['password2'];

        $status = 1;
        $userRole = $data['userRole'];

        #validate
        if ($password == $password2) {
            $password = $password2;
        } else {
            return $this->view->render($response, 'auth/signup.html');
        }

        # check if user exists
        $exists = $user->user_exists($username);
        if (!$exists) {            
            $user->register($username, $firstName, $lastName, $email, $password, $userRole, $status);

            # redirect to home
            echo "Success creating user";
            $this->flash->addMessage('success', 'User added successfully');

            return $response->withRedirect('/admin/users', 302);
        } else {
            # flash message for failure
            echo "Error creating user";
            $this->flash->addMessage('error', 'Error adding user');

            return $response->withRedirect($response, '/admin/users', 302);
        }
    });

    /**
     * updating user
     */
    $app->post('/users/update', function ($request, $response) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

      $user = new User($this->db);
      $data = $request->getParsedBody();

      $user_id = $data['user_id'];
      $username = $data['username'];
      $email = $data['email'];
      $firstName = $data['firstName'];
      $lastName = $data['lastName'];
    //   $password = $data['password'];
    //   $password2 = $data['password2'];

      $status = $data['status'];
      $userRole = $data['userRole'];

    //   #validate
    //   if ($password == $password2) {
    //     $password = $password2;
    //   } else {
    //     return $this->view->render($response, 'auth/signup.html');
    //   }

      # check if user exists
      $exists = $user->user_exists($username);
      if ($exists) {
        $user->update($user_id, $firstName, $lastName, $email, $userRole, $status);

        # redirect to admin users
        echo "Success updating user";
        $this->flash->addMessage('success', 'User edited successfully');

        return $response->withRedirect('/admin/users', 302);
      } else {
        # flash message for failure
        echo "Error updating user";
        $this->flash->addMessage('error', 'error updating user');
        return $response->withRedirect('/admin/users', 302);
      }
    });

    $app->post('/users/delete', function($request, $response)
    {   
        $user = new User($this->db);
        $data = $request->getParsedBody();
        $user_id = $data['user_id'];
        $username = $data['username'];

        # check if user exists
        $exists = $user->user_exists($username);
        if ($exists) {
            $user->delete($user_id);

            # redirect to admin users
            echo "Success deleting user";
            $this->flash->addMessage('success', 'user deleted successfully');

            return $response->withRedirect('/admin/users', 302);
        } else {
            # flash message for failure
            echo "Error updating user";
            $this->flash->addMessage('error', 'error deleting user');
            return $response->withRedirect('/admin/users', 302);
        }

        $User = new User($this->db);
        $User->delete($user_id);
    });

    /**
     * list all messages
     */
    $app->get('/messages', function($request, $response, $arguments) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $Message = new Message($this->db);
        $data = $Message->getMessages();
        $messages = $this->flash->getMessages();

        $context = array(
            'data' => $data,
            'messages' => $messages
        );

        return $this->view->render($response, 'admin/messages.html', $context);
        // echo json_encode($data);
    });

    /**
     * update message
     */
    $app->post('', function ($request, $response) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $this->logger->addInfo('Log whatever here!');
        $data = $request->getParsedBody();

        $message = filter_var($data['message'], FILTER_SANITIZE_STRING);
        $message_id = $data['message_id'];

        $Message = new Message($this->db);
        $Message->update($message_id, $message);

        $this->flash->addMessage('success', 'Message edited successfully');

        return $response->withStatus(302)
                        ->withHeader('Location', '/admin/message');
    });

    /**
     * delete message
     */
    $app->post('/messages/delete', function($request, $response, $arguments) 
    {
        // $User = new User($this->db);
        // if (!$User->isLoggedIn($_SESSION)) {
        //     $this->flash->addMessage('error', 'You must be logged in to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        // $user_id = (int)$_SESSION['id'];
        // if (!$User->isAdmin($user_id)) {
        //     $this->flash->addMessage('error', 'You must be admin to access');
            
        //     return $response->withRedirect('/auth/login', 302);
        // }

        $Message = new Message($this->db);
        $data = $request->getParsedBody();
        $message_id = $data['message_id'];

        $Message->delete($message_id);

        $this->flash->addMessage('success', 'Message deleted successfully');

        return $response->withRedirect('admin/messages', 302);
        // echo json_encode($data);
    });
    
});