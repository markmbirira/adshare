<?php

class AdvertInterest {

    private $db;

    public function __construct(PDO $conn) 
    {
        $this->db = $conn;
    }


    public function like($advert_id, $user_id, $status)
    {
        $sql = "INSERT INTO advert_interest (advert_id, user_id, status) VALUES (:advert_id, :user_id, :user_id)";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(":advert_id", $advert_id, PDO::PARAM_INT);
        $conn->bindValue(":user_id", $user_id, PDO::PARAM_INT);

        try {
            $result = $conn->execute();
            return $result;
        } catch (PDOException $e) {
            echo $e->getMessage();
            die('Error creating like');
        }
    }

    public function dislike($like_id, $advert_id, $user_id, $status) 
    {
        $sql = "UPDATE advert_interest SET (advert_id, user_id, status) VALUES (:advert_id, :user_id, :user_id) WHERE $like_id = :like_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(":advert_id", $advert_id, PDO::PARAM_INT);
        $conn->bindValue(":user_id", $user_id, PDO::PARAM_INT);
        $conn->bindValue(":like_id", $like_id, PDO::PARAM_INT);
        $conn->bindValue(":status", $status, PDO::PARAM_STR);

        try {
            $result = $conn->execute();
            return $result;
        } catch (PDOException $e) {
            echo $e->getMessage();
            die('Error creating like');
        }
    }
}