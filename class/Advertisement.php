<?php

// include('../models/config.php');

class Advertisement {

    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    public function createPost($title, $path, $description, $category_id, $user_id, $created_time, $status, $votes, $file_type) {
        $sql = "INSERT INTO advertisement 
                (title, path, description, category_id, user_id, created_time, status, votes, file_type) 
                VALUES (:title, :path, :description, :category_id, :user_id, NOW(), :status, :votes, :file_type
            )";
        $conn = $this->db->prepare($sql);

        $conn->bindValue(":title", $title, PDO::PARAM_STR);
        $conn->bindValue(":path", $path, PDO::PARAM_STR);
        $conn->bindValue(":description",$description, PDO::PARAM_STR);
        $conn->bindValue(":category_id", $category_id, PDO::PARAM_INT);
        $conn->bindValue(":user_id", $category_id, PDO::PARAM_INT);
        // $conn->bindValue(":created_time", $created_time, PDO::PARAM_STR);
        $conn->bindValue(":status", $status, PDO::PARAM_STR);
        $conn->bindValue(":votes", $votes, PDO::PARAM_INT);
        $conn->bindValue(":file_type", $file_type, PDO::PARAM_STR);

        try {
            $conn->execute();
            echo "Success Adding advert";
        } catch (PDOException $e) {
            echo "Error Occured!";
            echo $e->getMessage();
        }
    }

    public function listPosts($status) {

        $sql = "SELECT * FROM advertisement WHERE status = :status  ORDER BY id DESC";
        
        $conn = $this->db->prepare($sql);
        $conn->bindValue(':status', $status, PDO::PARAM_INT);

        try {
            $conn->execute();
            $data = $conn->fetchAll();
            return $data;

        } catch (PDOException $e) {
            "Error reading adverts";
            echo $e->getMessage();
        }
    }

    public function listUserPosts($user_id) {
        $sql = "SELECT * FROM advertisement WHERE user_id = :user_id ORDER BY id DESC";
        $conn = $this->db->prepare($sql);
        $user_id = (int)$user_id;
        $conn->bindValue(":user_id", $user_id, PDO::PARAM_INT);

        try {
            $conn->execute();
            $result = $conn->fetchAll();
            
            return $result;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function advertDetail($advert_id) {
        $sql = "SELECT * FROM advertisement WHERE id = :advert_id ORDER BY id DESC LIMIT 1";
        $conn = $this->db->prepare($sql);
        $advert_id = (int)$advert_id;
        $conn->bindValue(":advert_id", $advert_id, PDO::PARAM_INT);

        try {
            $conn->execute();
            $result = $conn->fetchAll();
            return $result;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function updatePost($advert_id, $title, $description, $category_id, $user_id, $created_time, $status, $votes) {
        $sql = "UPDATE advertisement SET
                title = :title, description = :description,
                category_id = :category_id, user_id = :user_id, created_time = :created_time,
                status = :status, votes = :votes
                WHERE id = :advert_id
            ";
        $conn = $this->db->prepare($sql);

        $conn->bindValue(":advert_id", $advert_id, PDO::PARAM_INT);
        $conn->bindValue(":title", $title, PDO::PARAM_STR);
        // $conn->bindValue(":path", $path, PDO::PARAM_STR);
        $conn->bindValue(":description", $description, PDO::PARAM_STR);
        $conn->bindValue(":category_id", $category_id, PDO::PARAM_INT);
        $conn->bindValue(":user_id", $category_id, PDO::PARAM_INT);
        $conn->bindValue(":created_time", $created_time, PDO::PARAM_STR);
        $conn->bindValue(":status", $status, PDO::PARAM_STR);
        $conn->bindValue(":votes", $votes, PDO::PARAM_INT);
        // $conn->bindValue(":file_type", $file_type, PDO::PARAM_STR);

        try {
            $conn->execute();
            echo "Success Updating  Advert";
        } catch (PDOException $e) {
            echo "Error Updating Occured!";
            echo $e->getMessage();
        }
    }

    public function delete($status, $advert_id) {
        try {
            $sql = "UPDATE advertisement SET status = :status WHERE id = :advert_id";
            $conn = $this->db->prepare($sql);
        
            $conn->bindValue(":advert_id", $advert_id, PDO::PARAM_INT);
            $conn->bindValue(":status", $status, PDO::PARAM_INT);
        
            $result = $conn->execute();
            return $result;
        
            }  catch(PDOException $e) {
                echo "Error deleting advert";
               $e->getMessage();
            }
    }

    public function favouriteAd($advert_id, $votes) {
        try {
            $sql = "UPDATE advertisement SET votes=:votes WHERE id = :advert_id";
            $conn = $this->db->prepare($sql);
      
            $conn->bindValue(":advert_id", $advert_id, PDO::PARAM_INT);
            $conn->bindValue(":votes", $votes, PDO::PARAM_INT);
      
            $result = $conn->execute();
            return $result; 

        } catch (PDOException $e) {
            echo "Error favouriting article";
            $e->getMessage();
        }
    }
}

/**
 * Testing Advertising Model
 */

// $adv = new Advertisement($db);

// $title = "title";
// $path = "path/title/photo.png";
// $description = "Post description Here!";
// $user_id = 1;
// $category_id = 1;
// $votes = 0;
// $file_type = 'photo';
// $status = 1;
// $created_time = date('Y:m:d H:i:s');

// $adv->createPost($title, $path, $description, $category_id, $user_id, $created_time, $status, $votes, $file_type);

// $adv->updatePost(1, $title, $path, $description, $category_id, $user_id, $created_time, $status, $votes, $file_type);
// $adv->favouriteAd(1, 12);
// $adv->delete(0, 1);

// $adverts = $adv->listUserPosts(1);

// var_dump($adverts);

