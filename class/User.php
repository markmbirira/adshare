<?php

include_once('../models/config.php');

class User {
  private $db;

  public function __construct(PDO $db) {
    $this->db = $db;
  }

  public function register($username, $firstName, $lastName, $email, $password, $userRole, $status) 
  {

  	try {
  		$sql = "INSERT INTO users (username, first_name, last_name, email, password, memberSince, userRole, status) values (:username, :firstName, :lastName, :email, :password, NOW(), :userRole, :status)";
  		$conn = $this->db->prepare($sql);
      $conn->bindValue(":username", $username, PDO::PARAM_STR);
      $conn->bindValue(":firstName", $firstName, PDO::PARAM_STR);
      $conn->bindValue(":lastName", $lastName, PDO::PARAM_STR);
      $conn->bindValue(":email", $email, PDO::PARAM_STR);
      $conn->bindValue(":password", hash('SHA256', $password), PDO::PARAM_STR);
      $conn->bindValue(":userRole", $userRole, PDO::PARAM_STR);
      $conn->bindValue(":status", $status, PDO::PARAM_INT);

      $result = $conn->execute();
      echo "Success registering user";
      return $result;
    }  catch(PDOException $e) {
      echo "Error registering users";
      echo $e->getMessage();
    }
  }

  public function update($user_id, $firstName, $lastName, $email) 
  {
  	try {
      $sql = "UPDATE users SET first_name = :firstName, last_name = :lastName,  email = :email WHERE id = :user_id";
      
      $conn = $this->db->prepare($sql);
      $conn->bindValue(":user_id", $user_id, PDO::PARAM_STR);
      $conn->bindValue(":firstName", $firstName, PDO::PARAM_STR);
      $conn->bindValue(":lastName", $lastName, PDO::PARAM_STR);
      $conn->bindValue(":email", $email, PDO::PARAM_STR);

      $result = $conn->execute();
      echo "Success Updating user";
      return $result;
    }  catch(PDOException $e) {
      echo "Error registering users";
      echo $e->getMessage();
    }
  }

  public function setProfilePicture($user_id, $path)
  {
    $sql = "UPDATE users SET path = :path WHERE id = :user_id";

    $conn = $this->db->prepare($sql);
    $conn->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    $conn->bindValue(':path', $path, PDO::PARAM_STR);

    try {
      $conn->execute();
      $result = $conn->fetchAll();
      echo "Profile picture added successfully.";
    
      return $result;
    } catch (PDOException $e) {
      echo "Error updating profile picture";
      echo $e->getMessage();
    }
  }

  public function delete($user_id)
  {
    $sql = "UPDATE users SET status = 0 WHERE id = :user_id";

    $conn = $this->db->prepare($sql);
    $conn->bindValue(':user_id', $user_id, PDO::PARAM_STR);

    try {
      $conn->execute();
      $result = $conn->fetchAll();
      return result;
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function user_exists($username) 
  {
    $sql = " SELECT username FROM users WHERE username = :username LIMIT 1";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(":username",$username,PDO::PARAM_STR);
    $conn->execute();
    return $conn->fetchColumn();

  }

  public function login($username, $password) 
  {
  	try {
      $sql = "SELECT username, password FROM users where username = :username AND password = :password LIMIT 1";
      $conn = $this->db->prepare($sql);

      $conn->bindValue(":username", $username, PDO::PARAM_STR);
      $conn->bindValue(":password", hash('SHA256',$password), PDO::PARAM_STR);

      
      $conn->execute();
      $result = $conn->fetchColumn();

      /* returns false on failure and the table row on success */
      return $result;

    }  catch (PDOException $e) {
      echo "Login PDO Error";
    	$e->getMessage();
    }
  }

  public function getUser($username)
  {
    $sql = "SELECT * FROM users WHERE id = 1";

    $conn = $this->db->prepare($sql);
    $conn->bindValue(':username', $username, PDO::PARAM_INT);

    try {
      $conn->execute();
      $result = $conn->fetchAll();
      return $result;
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function getUsers($exclude_id = NULL)
  {
    if ($exclude_id != NULL)
    {
      $sql = "SELECT * FROM users WHERE id != :exclude_id";
      $conn = $this->db->prepare($sql);
      $conn->bindValue(":exclude_id", $exclude_id, PDO::PARAM_STR);
    }
    else
    {
      $sql = "SELECT * FROM users ORDER BY ID DESC";
      $conn = $this->db->prepare($sql);
    }
   
    
    try {
      $conn->execute();
      $result = $conn->fetchAll();
      return $result;
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function getActiveUsers()
  {
    $sql = "SELECT * FROM users WHERE status = :status";

    $conn = $this->db->prepare($sql);
    $conn->bindValue(':status', 1, PDO::PARAM_INT);
  
    try {
      $conn->execute();
      $result = $conn->fetchAll();
      return $result;
    }
    catch (PDOException $e)
    {
      echo $e->getMessage();
    }
  }

  public function isLoggedIn()
  {
    if (!empty($_SESSION['username']))
    {
      return true;
    } else {
      return false;
    }
  }

  public function isAdmin($user_id)
  {
    $sql = "SELECT * FROM users WHERE id = :id AND userRole = :userRole";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(':id', $user_id, PDO::PARAM_INT);
    $conn->bindValue(':userRole', "admin", PDO::PARAM_STR);

    try {
      $conn->execute();
      $result = $conn->fetchColumn();

      if ($result) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e)
    {
      echo "Error checking admin";
    }
  }

}

// $user = new User($db);

// $username = 'Ad';
// $status = 1;
// $user_id = 4;

// // $profile = $user->getUser($username);

// $update = $user->update(1, "Mark", "Mbirira", "markmbirira@gmail.com");

// var_dump($update);

