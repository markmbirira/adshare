
<?php

# start session
session_start();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../assets/config.php';

$app = new \Slim\App(["settings" => $config]);

# Autoload custom classes
spl_autoload_register(function ($classname) {
  require ("../class/" . $classname . ".php");
});

# App dependency injection (DIC)
# Database Connection (PDO) and
# Logging (Monolog)

$container = $app->getContainer();

$container['logger'] = function ($c) {
  $logger = new \Monolog\Logger("my_logger");
  $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
  $logger->pushHandler($file_handler);
  return $logger;
};

$container['db'] = function ($c) {
  $db = $c['settings']['db'];
  $pdo = new PDO("mysql:host=" .$db['host'] . ";dbname=" . $db['dbname'],$db['user'], $db['pass']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $pdo;
};

$container['view'] = function ($container) {
  $view = new \Slim\Views\Twig('../templates', [
    'cache' => false, 'auto_reload' => true
  ]);
  // $view->addGlobal('session', $_SESSION);
  $view->addExtension(new \Slim\Views\TwigExtension(
    $container['router'],
    $container['request']->getUri()
  ));

  return $view;
};

$container['flash'] = function ($container) {
  $flash =  new \Slim\Flash\Messages();
  return $flash;
};

# configure application middleware

# load App routes;
require '../routes/adminRoutes.php';
require '../routes/advertRoutes.php';
require '../routes/messageRoutes.php';
require '../routes/Routes.php';

$app->run();

?>
