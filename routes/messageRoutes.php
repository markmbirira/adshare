<?php

$app->group("/message", function() use ($app) {

    // compose message
    $app->get('', function ($request, $response, $args) {
        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $User = new User($this->db);
        $user_id = $_SESSION['id'];

        $all_users = $User->getUsers($user_id);
        $current_user = $User->getUser($user_id);

        $context = array(
            'all_users' => $all_users,
            'current_user' => $current_user,
            'categories' => $categories,
        );
        
        return $this->view->render($response, 'message/compose.html', $context);
    });

    $app->post('', function ($request, $response) {
        // $this->logger->addInfo('Log whatever here!');
        $data = $request->getParsedBody();

        $message = filter_var($data['message'], FILTER_SANITIZE_STRING);
        $user_from = $data['user_from'];
        $user_to = $data['user_to'];
        $seen_status = 1;

        $Message = new Message($this->db);
        $Message->send($message, $user_from, $user_to, $seen_status);
  
        # Send notification after user receives message
        $Notification = new Notification($this->db);
        $Notification->setNotification($user_to, 'You have a new message', 0);
        
        $thread_link = '/message/' . $user_from . '/thread/' . $user_to;
        return $response->withStatus(302)
                        ->withHeader('Location', $thread_link);
    });
    
    # get user messages/threads
    $app->get('/all/{user_id}', function($request, $response, $args) {
        $Message = new Message($this->db);
        $user_id = (int)$args['user_id'];

        $data = $Message->getMessages($user_id);

        $User = new User($this->db);
        $current_user = ($User->getUser($user_id))[0];

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $context = array(
            'data' => $data,
            'current_user' => $current_user,
            'categories' => $categories,
        );

        return $this->view->render($response, 'message/view_messages.html', $context);
        // echo json_encode($data);
    });

    # get single thread
    $app->get('/{user_id}/thread/{user_to_id}', function($request, $response, $args) {
        $Message = new Message($this->db);
        $user_id = (int)$args['user_id'];
        $user_to_id = (int)$args['user_to_id'];
        $data = $Message->getThread($user_id, $user_to_id);

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $User = new User($this->db);
        $thread_user = $User->getUser($user_id);

        var_dump($thread_user);

        $context = array(
            'data' => $data,
            'categories' => $categories,
            'thread_user' => $thread_user
        );

        return $this->view->render($response, 'message/view_thread.html', $context);
        // echo json_encode($data);
    });

    /*
     * get single message
     */
    $app->get('/detail/{message_id}', function($request, $response, $args) {
        $Message = new Message($this->db);
        $message_id = (int)$args['message_id'];
        $data = $Message->readMessage($message_id);

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $User = new User($this->db);
        $message_user_to = $User->getUser($user_id);

        $context = array(
            'data' => $data,
            'categories' => $categories,
            'message_user_to' => $message_user_to,
        );

        return $this->view->render($response, 'advert/detail.html', $context);
        // echo json_encode($data);
    });
    
    /**
     *  delete
     */
    $app->get('delete/{id}', function ($request, $response, $args) {
        
    
        # redirect after inserting
        return $response->withRedirect('/message', 302);
    });
    
});