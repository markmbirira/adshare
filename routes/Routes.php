<?php

# routes for articles
$app->group('/', function () use ($app) {
  $app->get('', function ($request, $response) {
      return $this->view->render($response, 'landing.html');
  });

  $app->post('notifications', function ($request, $response) 
  {
      $data = $request->getParsedBody();
      $user_to = $data['user_to'];

      $Notification = new Notification($this->db);
      $notifications = $Notification->hasNotifications($user_to);

      # encode JSON
      $results = json_decode($notifications);
      echo $results;
  });
});

# user management groups

$app->group('/auth/', function () use($app) {

  /**
   * Destroy current session.
   */
  $app->get('logout', function ($request, $response, $args) {
      $_SESSION['username'] = NULL;
      $_SESSION['id'] = NULL;

      $this->flash->addMessage('success', 'Logged out successfully!');
      
      return $response->withRedirect('/auth/login');
  });

  $app->group('login', function () use ($app) {
    $app->get('', function($request, $response) {

      $Category = new Category($this->db);
      $categories = $Category->getCategories();

      return $this->view->render($response, 'auth/login.html', ['categories' => $categories]);
    });

    $app->post('', function ($request, $response) {
      $user = new User($this->db);
      $data = $request->getParsedBody();
      $username = $data['username'];
      $password = $data['password'];

      # log the user in
      $login = $user->login($username, $password);

      if ($login) {
        /**
         * Session configuration
         */
        $User = new User($this->db);
        $current_user = $User->getUser($username);

        # session
        $_SESSION['username'] = $username;

        $this->flash->addMessage('success', 'welcome ' . $username);

        return $response->withRedirect('/', 302);
      } else {
        # flash message
        return $this->view->render($response, 'auth/login.html');
      }
    });
  });

  $app->group('signup', function () use ($app) {
    $app->get('', function ($request, $response) {

      $Category = new Category($this->db);
      $categories = $Category->getCategories();

      return $this->view->render($response, 'auth/signup.html', ['categories' => $categories]);
    });

    $app->post('', function ($request, $response, $args) {
      $user = new User($this->db);
      $data = $request->getParsedBody();

      $username = $data['username'];
      $email = $data['email'];
      $firstName = $data['firstName'];
      $lastName = $data['lastName'];
      $password = $data['password'];
      $password2 = $data['password2'];

      $status = 0;
      $userRole = 'user';

      #validate
      if ($password == $password2) {
        $password = $password2;
      } else {
        return $this->view->render($response, 'auth/signup.html');
      }

      # check if user exists
      $exists = $user->user_exists($username);
      if (!$exists) {
        $user->register($username, $firstName, $lastName, $email, $password, $userRole, $status);

        # get current user
        $User = new User($this->db);
        $current_user = $User->getUser($username);

        # session
        $_SESSION['username'] = $current_user[0]['username'];
        $_SESSION['id'] = $current_user[0]['id'];

        # redirect to home
        return $response->withRedirect('/advert', 302);
      } else {
        # flash message for failure
        return $this->view->render($response, 'auth/signup.html');
      }

    });

  }); # closing signup management

}); # closing user management route group

/** User profile */
$app->group('/profiles', function () use ($app) 
{
  /**
   * List all users
   * 
   * @param \Slim\Http\Request        $request    HTTP request object
   * @param \Slim\Http\Response       $response   HTTP response object
   * @param array                     $args       GET request parameters
   * 
   * @return \Slim\Http\Response      $response   processed HTTP response object
   */
  $app->get('', function($request, $response, $args)
  {
    $User = new User($this->db);
    $users = $User->getUsers();

    $context = array(
      'users' => $users,
    );

    return $this->view->render($response, 'auth/profile.html', $context);
  });

  /**
   *  Show user profile
   * 
   * @param \Slim\Http\Request        $request    HTTP request object
   * @param \Slim\Http\Response       $response   HTTP response object
   * @param array                     $args       GET request parameters
   * 
   * @return \Slim\Http\Response      $response   processed HTTP response object
   */
  $app->get('/{username}', function($request, $response, $args)
  {
    $username = $args['username'];
    $User = new User($this->db);
    $current_user = $User->getUser($username);
    $user_id = $current_user[0]['id'];

    $Advert = new Advertisement($this->db);
    $user_adverts = $Advert->listUserPosts($user_id);

    $messages = $this->flash->getMessages();

    $context = array(
      'current_user' => $current_user,
      'user_adverts' => $user_adverts,
      'messages' => $messages,
    );

    return $this->view->render($response, 'auth/profile.html', $context);

  });
  
  /**
   * Update user profile
   * 
   * @param \Slim\Http\Request        $request    HTTP request object
   * @param \Slim\Http\Response       $response   HTTP response object
   * 
   * @return \Slim\Http\Response      $response   processed HTTP response object
   */
  $app->post('/update', function ($request, $response) 
  {
    $User = new User($this->db);
    $data = $request->getParsedBody();

    $user_id = $data['user_id'];
    $current_user = $User->getUser($user_id);
    $username = $current_user[0]['username'];
    $email = $data['email'];
    $firstName = $data['firstName'];
    $lastName = $data['lastName'];
   
    # check if user exists
    $exists = $User->user_exists($username);
    if ($exists) {
      $User->update($user_id, $firstName, $lastName, $email);

      # redirect to admin users
      $this->flash->addMessage('success', 'profile updated');

      return $response->withRedirect('/profiles/' . $current_user[0]['username'], 302);
    } else {
      # flash message for failure
      $this->flash->addMessage('success', 'Error updating profile');

      $Advert = new Advertisement($this->db);
      $user_adverts = $Advert->listUserPosts($user_id);
      $messages = $this->flash->getMessages();
      
      $context = array(
        'current_user' => $current_user,
        'user_adverts' => $user_adverts,
        'messages' => $messages,
      );
      return $this->view->render($response, 'auth/profile.html', $context);
    }
  });

  $app->post('/update/avatar', function ($request, $response, $args) 
  {
    $data = $request->getParsedBody();
    $files = $request->getUploadedFiles();

    $file = $files['path'];
    if ($file->getError() === UPLOAD_ERR_OK or NULL) 
    {
      $file_name = $file->getClientFilename();
      $type = $file->getClientMediaType();
      $path = "/uploads/$file_name";
      $file->moveTo("uploads/$file_name");
    } else {
      $path = "";
    }

    $user_id = $data['user_id'];
    $username = $data['username'];
    $User = new User($this->db);

    $exists = $User->user_exists($username);
    if ($exists) {
      $User->setProfilePicture($user_id, $path);
      $this->flash->addMessage('success', 'Profile picture updated');
      $success = array('status' => 'ok');
      // echo json_encode($success);
      return $response->withRedirect('/profiles/'.$username,  302);

    } else {
      $this->flash->addMessage('success', 'Error updating profile picture');
      $error = array('status' => 'ko');
      // echo json_encode($error);
      return $response->withRedirect('/profiles/'.$username,  302);
    }
    
  });
});

?>
