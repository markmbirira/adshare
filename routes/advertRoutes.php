<?php

$app->group("/advert", function() use ($app) {
    
    /**
     * List all posts
     */
    $app->get('', function($request, $response, $arguments) 
    {
        $article = new Advertisement($this->db);
        $data = $article->listPosts(1);

        $User = new User($this->db);
        if (!$User->isLoggedIn($_SESSION)) {
            $this->flash->addMessage('error', 'You must be logged in to access');
            header('Location:/auth/login');
        }

        $user_id = (int)$_SESSION['id'];

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        $Notification = new Notification($this->db);
        $notifications = $Notification->hasNotifications($user_id);

        // var_dump($notifications);

        $context = array(
            'data' => $data,
            'categories' => $categories ,
            'notifications' => $notifications
        );

        return $this->view->render($response, 'advert/list.html', $context);
        // echo json_encode($data);
    });

    /**
     * Show single post
     */
    $app->get('/detail/{advert_id}', function($request, $response, $args) 
    {
        $User = new User($this->db);
        if (!$User->isLoggedIn($_SESSION)) {
            $this->flash->addMessage('error', 'You must be logged in to access');
            header('Location:/auth/login');
        }

        $article = new Advertisement($this->db);
        $advert_id = $args['advert_id'];
        $data = $article->advertDetail($advert_id);

        $Category = new Category($this->db);
        $categories = $Category->getCategories();

        return $this->view->render($response, 'advert/detail.html', ['data' => $data, 'categories' => $categories]);
        // echo json_encode($data);
    });

    /**
     * Create advert
     */
    $app->group('/create', function () use($app) {
        $app->get('', function ($request, $response) {
            $User = new User($this->db);
            if (!$User->isLoggedIn($_SESSION)) {
                $this->flash->addMessage('error', 'You must be logged in to access');
                header('Location:/auth/login');
            }

            $this->logger->addInfo("Link for a new post");

            $User = new User($this->db);
            if (!$User->isLoggedIn()) {
                $this->flash->addMessage('error', 'You must be logged in to access');
                header('Location:/auth/login');
            }

            $Category = new Category($this->db);
            $categories = $Category->getCategories();

            return $this->view->render($response, 'advert/create.html', ['categories' => $categories]);
        });

        $app->post('/new', function ($request, $response) {
            // $this->logger->addInfo('Log whatever here!');
            $User = new User($this->db);
            if (!$User->isLoggedIn($_SESSION)) {
                $this->flash->addMessage('error', 'You must be logged in to access');
                header('Location:/auth/login');
            }

            $data = $request->getParsedBody();
            $files = $request->getUploadedFiles();

            #uploaded file
            $file = $files['path'];

            if ($file->getError() === UPLOAD_ERR_OK or NULL) {
              $file_name = $file->getClientFilename();
              $file_type = $file->getClientMediaType();
              $path = "/uploads/$file_name";
              $file->moveTo("uploads/$file_name");
            } else {
              $file_type = null;
              $path = null;
            }
        
      
            $title = filter_var($data['title'], FILTER_SANITIZE_STRING);
            $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $category_id = $data['category_id'];
            $created_time = date('Y:m:d H:i:s');
            $status = 1;
            $votes = 0;

            $user_id = (int)$_SESSION['id'];

            // die();
            $article = new Advertisement($this->db);
            $article->createPost($title, $path, $description, $category_id, $user_id, $created_time, $status, $votes, $file_type);
      
            # redirect after inserting
            $message = $this->flash->addMessage('advertisement', 'Advertisement added successfully');
      
            return $response->withStatus(302)
                            ->withHeader('Location', '/advert');
        });

    });

    $app->group('/update', function () use($app) {
        $app->post('', function ($request, $response) 
        {
            $User = new User($this->db);
            if (!$User->isLoggedIn($_SESSION)) {
                $this->flash->addMessage('error', 'You must be logged in to access');
                header('Location:/auth/login');
            }

          $Advertisement = new Advertisement($this->db);

          $data = $request->getParsedBody();
          $files = $request->getUploadedFiles();
        
            #uploaded file
            $file = $files['path'];

            if ($file->getError() === UPLOAD_ERR_OK or NULL) {
                $file_name = $file->getClientFilename();
                $file_type = $file->getClientMediaType();
                $path = "/uploads/$file_name";
                $file->moveTo("uploads/$file_name");
            } else {
                $file_type = null;
                $path = null;
            }
    
          $post_id = $data['post_id'];
          $advert = $Advertisement->advertDetail($post_id);
        
          $user_id = $advert[0]['id'];

          $title = filter_var($data['title'], FILTER_SANITIZE_STRING);
          $description = filter_var($data['description'], FILTER_SANITIZE_STRING);
          $category_id = $data['category_id'];
          $created_time = date('Y:m:d H:i:s');
          $status = 1;
          $votes = 0;
    
          $Advertisement->updatePost($post_id, $title, $path, $description, $category_id, $user_id, $created_time, $status, $votes, $file_type);

    
          # redirect after inserting
          $this->flash->addMessage('Success', 'Advert updated successfully');
          return $response->withRedirect('/advert/detail/' . $post_id, 302);
        });

        $app->get('/{id}', function ($resquest, $response, $args) 
        {
            $User = new User($this->db);
            if (!$User->isLoggedIn($_SESSION)) {
                $this->flash->addMessage('error', 'You must be logged in to access');
                header('Location:/auth/login');
            }

          $Advertisement = new Advertisement($this->db);
          # collect data to populate the form with
          $id = $args['id'];
          $data = $Advertisement->advertDetail($id);

          $Category = new Category($this->db);
          $categories = $Category->getCategories();

          $messages = $this->flash->getMessages();

          print_r($messages);
    
          $context = array(
            "data" => $data,
            "categories" => $categories,
            "messages" => $messages
          );
          return $this->view->render($response, 'advert/update.html', $context);
        });
    });

    $app->get('delete/{id}', function ($request, $response, $args)
    {
        $User = new User($this->db);
        if (!$User->isLoggedIn($_SESSION)) {
            $this->flash->addMessage('error', 'You must be logged in to access');
            header('Location:/auth/login');
        }

        $advert = new Advertisement($this->db);
        $id = $args['id'];
        $status = true;
    
        $advert->delete($id, $status);
    
        # redirect after inserting
        return $response->withRedirect('/', 302);
    });
    
    # interested
    $app->get('like/{id}', function ($request, $response, $args)
    {
        $User = new User($this->db);
        if (!$User->isLoggedIn($_SESSION)) {
            $this->flash->addMessage('error', 'You must be logged in to access');
            header('Location:/auth/login');
        }

        $Advert = new Advertisement($this->db);
        $id = $args['id'];
        $current_advert = $Advert->findOne($id);
        
        # redirect after inserting
        return $response->withRedirect('/', 302);
    });
    
    # remove interested
    $app->get('dislike/{id}', function ($request, $response, $args) {

    });
});