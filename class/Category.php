<?php

require_once('../models/config.php');

class Category {

    private $db;

    public function __construct(PDO $conn)
    {
        $this->db = $conn;
    }

    public function getCategories()
    {
        $sql = "SELECT * FROM category WHERE status = 1";

        $conn = $this->db->prepare($sql);

        try
        {
            $conn->execute();
            $result = $conn->fetchAll();
            // echo "Success getting categories";
            return $result;
        }
        catch (PDOException $e)
        {   
            echo "Error getting categories";
            echo $e->getMessage();
        }

    }

    public function setCategory($name, $description, $created_date)
    {
        $sql = "INSERT INTO category (name, description, created_date, status) 
                VALUES (:name, :description, :created_date, 1)";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':name', $name, PDO::PARAM_STR);
        $conn->bindValue(':description', $description, PDO::PARAM_STR);
        $conn->bindValue(':created_date', $created_date);

        try 
        {
            $result = $conn->execute();
            // echo "Success setting categories";
        }
        catch (PDOException $e)
        {
            echo "Error setting categories";
            echo $e->getMessage();
            die('Error setting categories');
        }
    }

    public function updateCategory($category_id, $name, $description)
    {
        $sql = "UPDATE category SET name = :name, description = :description WHERE id = :category_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':name', $name, PDO::PARAM_STR);
        $conn->bindValue(':description', $description, PDO::PARAM_STR);
        $conn->bindValue(':category_id', $category_id);

        try 
        {
            $result = $conn->execute();
            // echo "Success setting categories";
        }
        catch (PDOException $e)
        {
            echo "Error setting categories";
            echo $e->getMessage();
            die('Error setting categories');
        }
    }

    public function deleteCategory($category_id, $status)
    {
        $sql = "UPDATE category SET status = :status WHERE id = :category_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':status', $status, PDO::PARAM_STR);
        $conn->bindValue(':category_id', $category_id, PDO::PARAM_INT);

        try 
        {
            $result = $conn->execute();
            // echo "Success setting categories";
        }
        catch (PDOException $e)
        {
            echo "Error setting categories";
            echo $e->getMessage();
            die('Error setting categories');
        }
    }
}


/**
 * Test Category model
 */
//  $now = date('Y:m:d H:i:s');
//  $name = "electronics";
//  $description = "Electronic Products for second hand ownership of products";

//  $cat = new Category($db);

//  $cat->setCategory($name, $description, $now);

//  $cats = $cat->getCategories();

//  var_dump($cats);

