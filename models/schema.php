<?php

class Minitweet {

  private $db;
  private $tableCount;

  public function __construct(PDO $db) {
    $this->db = $db;
    $this->tableCount = 0;
  }

  public function incrementTables() {
    $this->tableCount++;
  }

  public function getTables() {
    return $this->tableCount;
  }

  public function user() {
    $sql  = "CREATE TABLE IF NOT EXISTS users (
              id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
              username VARCHAR(100) NOT NULL UNIQUE,
              email VARCHAR(100) NULL,
              password VARCHAR(255) NOT NULL,
              memberSince DATETIME NOT NULL,
              userRole VARCHAR(20) NOT NULL,
              status BOOLEAN NOT NULL
    )";


    $conn = $this->db->prepare($sql);
    try {
      $conn->execute();
      
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    $this->incrementTables();
    echo "Table users created successfully...\t\t\t\t\t\t\t created ". $this->getTables() ."\n";

  }

  public function article() {
    $sql = "CREATE TABLE IF NOT EXISTS feeds (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            body VARCHAR(100) NOT NULL,
            timePosted DATETIME NOT NULL, # DEFAULT NOW(),
            status SMALLINT NOT NULL,
            author VARCHAR(100) NOT NULL,
            path VARCHAR(255) NULL,
            type VARCHAR(100) NULL,
            votes SMALLINT NULL,
            down_votes SMALLINT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    }  catch(PDOException $e) {
      echo $e->getMessage();
    }

    $this->incrementTables();
    echo "Table article created successfully...\t\t\t\t\t\t\t created ". $this->getTables() ."\n";

  }

  public function comment() {
    $sql = "CREATE TABLE IF NOT EXISTS comments (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            comment VARCHAR(255) NOT NULL,
            timePosted DATETIME NOT NULL, # DEFAULT NOW(),
            author VARCHAR(100) NOT NULL,
            article_id INT NOT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    $this->incrementTables();
    echo "Table comments created successfully...\t\t\t\t\t\t\t created ". $this->getTables() ."\n";

  }

  public function advertisement() {
    $sql = "CREATE TABLE IF NOT EXISTS advertisement (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR(255) NOT NULL,
            path VARCHAR(255) NOT NULL,
            description TEXT NOT NULL,
            category_id INT  NOT NULL,
            user_id INT NOT NULL,
            created_time DATETIME DEFAULT NOW(),
            status BOOLEAN NOT NULL,
            votes INT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    $this->incrementTables();
    echo "Table advetisement created successfully...\t\t\t\t\t\t\t created ". $this->getTables() ."\n";
  }

  public function advert_interest() {
    $sql = "CREATE TABLE IF NOT EXISTS advert_interest (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            advert_id INT NOT NULL,
            user_id INT NOT NULL,
            time_liked DATETIME,
            status BOOLEAN NOT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    $this->incrementTables();
    echo "Table like created successfully...\t\t\t\t\t\t\t created ". $this->getTables() ."\n";
  }

  public function message() {
    $sql = "CREATE TABLE IF NOT EXISTS message (
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            user_from INT NOT NULL, 
            user_to INT NOT NULL,
            message TEXT NOT NULL,
            time_sent DATETIME NOT NULL,
            seen BOOLEAN NOT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    $this->incrementTables();
    echo "Table message created successfully...\t\t\t\t\t\t\t created ". $this->getTables() ."\n";
  }

}

include "config.php";
$cbl = new Minitweet($db);

$cbl->user();
$cbl->article();
$cbl->comment();
$cbl->advertisement();
$cbl->advert_interest();
$cbl->message();

?>
