<?php

class Comment {
  private $db;

  public function __construct(PDO $db) {
    $this->db = $db;
  }

  public function create($comment, $user, $post_id) 
  {
    try {
    $sql = "INSERT INTO comments (comment, blogger, post_id, timePosted) values (:comment, :blogger, :post_id, NOW())";
    $conn = $this->db->prepare($sql);

    $conn->bindValue(":comment", $comment, PDO::PARAM_STR);
    $conn->bindValue(":blogger", $blogger, PDO::PARAM_STR);
    $conn->bindValue(":post_id", $post_id, PDO::PARAM_INT);

    $result =  $conn->execute();
    return $result;

    }  catch(PDOException $e) {
      $e->getMessage();
    }

  }

  public function read($id) {
    try {
    $sql = "SELECT * FROM comments WHERE post_id = :id ORDER BY id DESC";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(":id",$id, PDO::PARAM_INT);

    $conn->execute();
    $result = $conn->fetchAll();

    return $result;

    } catch (PDOException $e) {
      $e->getMessage();
    }

  }


  public function update($id, $comment) {
    try {
    $sql = "UPDATE comments SET comment = :comment WHERE id = :id";

    $conn = $this->db->prepare($sql);

    $conn->bindValue(":id", $id, PDO::PARAM_INT);
    $conn->bindValue(":comment", $comment, PDO::PARAM_STR);

    $result = $conn->execute();

    return $result;

      $conn->execute($values);
    } catch (PDOException $e) {
      $e->getMessage();
    }

  }

  /* Delete Posts and all their related comments */
  public function delete($id) {
    try {
    $sql = "DELETE FROM comments WHERE id = :id";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(":id", $id, PDO::PARAM_INT);

    $result = $conn->execute();

    return $result;

    }  catch(PDOException $e) {
      $e->getMessage();
    }

  }

  public function total_comment($post_id) {
    $sql = "SELECT count(id) FROM comments WHERE post_id = :post_id";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(":post_id", $post_id, PDO::PARAM_INT);
    $conn->execute();
    $result = $conn->fetchAll();
    return $result;
  }


}


?>
