$(document).ready(function() {
  //load the whole data when the page loads
  fetchAll();
})

function fetchAll() {
  $.ajax({
    type: 'GET',
    url: '/read',
    dataType: 'json',
    success: fetchAllSuccess,
    error: fetchAllError
  });
}

function fetchAllSuccess(response, status) {
    console.log(response);
    var html;
    response.map(function(article) {
        html = "<div class='feed-item'>"
        html += "<p class='feed-header'>" + article.author + " >> <span class='timePosted'>" + moment(article.timePosted).fromNow() + "</span>"
        + "<span class='edit'>&#9998;</span></p>";
        html += "<p class='feed-body'>" + article.body + "</p>";
        if(article.path != null) {
          if(article.type == "image/jpg" || article.type == "image/jpeg" || article.type== "image/png") {
              html += "<p class='fedd-media'><img src='"+article.path+"' alt='image' >";
          }
          else if(article.type == "video/mp4" || article.type == "video/flv" || article.type == "video/avi") {
              html += "<p class='feed-media'><video src='" + article.path + "' controls /></p>";
          }
        }

        html += "</div>";

        $(".feed").append(html);
    });
}

function fetchAllError(xhr, status, error) {
  alert("An error occured: ", errorThrown);
}

// file upload
$('#uploadbtn').click(function(){
  $('#file').toggleClass("show-upload");
  var toggle = false;
})
