<?php

// include('../models/config.php');

class Message {
    
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    public function send($message, $user_from, $user_to, $seen) 
    {
        $sql = "INSERT INTO message (message, user_from, user_to, time_sent, seen)
                VALUES (:message, :user_from, :user_to, NOW(), :seen)
        ";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':message', $message, PDO::PARAM_STR);
        $conn->bindValue(':user_from', $user_from, PDO::PARAM_INT);
        $conn->bindValue(':user_to', $user_to, PDO::PARAM_INT);
        $conn->bindValue(':seen', $seen, PDO::PARAM_INT);

        try
        {
            $result = $conn->execute();
            echo "Message sent successfully";
            return $result;
        } catch (PDOException $e)
        {
            echo "Error sending message";
            echo $e->getMessage();
        }
    }

    public function update($message_id, $message) 
    {
        $sql = "UPDATE message SET message = :message WHERE id = :message_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':message', $message, PDO::PARAM_STR);
        $conn->bindValue(':message_id', $message_id, PDO::PARAM_STR);

        try
        {
            $result = $conn->execute();
            echo "Message updated successfully";
            return $result;
        } catch (PDOException $e)
        {
            echo "Error updating message";
            echo $e->getMessage();
        }
    }


    public function getThread($user_from, $user_to)
    {
        $sql = "SELECT * FROM message WHERE user_from = :user_from AND user_to = :user_to";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':user_from', $user_from, PDO::PARAM_INT);
        $conn->bindValue(':user_to', $user_to, PDO::PARAM_INT);

        try
        {
            $conn->execute();
            $result = $conn->fetchAll();
            // echo "Success fetching messages";
            return $result;
        } catch (PDOException $e)
        {   
            // echo 'Error getting messages';
            echo $e->getMessage();
        }
        
    }

  
    public function seen($message_id, $seen)
    {
        $sql = "UPDATE message SET seen  = :seen, seen_time = NOW() WHERE id = :message_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(":seen", $seen, PDO::PARAM_INT);
        // $conn->bindValue(":seen_time", $seen_time, PDO::PARAM_STR);
        $conn->bindValue(":message_id", $message_id, PDO::PARAM_INT);

        try {
            $result = $conn->execute();

            # Create notification to notification table
            
            return $result;
        } catch (PDOException $e) {
            echo "Error setting seen on message";
            echo $e->getMessage();
        }
    }

    public function readMessage($message_id) {
        $sql = "SELECT * FROM message WHERE id = :message_id";
        $conn = $this->db->prepare($sql);
        $conn->bindValue(':message_id', $message_id, PDO::PARAM_STR);

        try
        {
            $conn->execute();
            $result = $conn->fetchAll();
            return $result;
        } catch (PDOException $e)
        {
            echo 'Error reading message';
            echo $e->getMessage();
        }
    }

    public function getMessages($user_id = NULL) {
        if ($user_id)
        {
            $sql = "SELECT * FROM message WHERE user_from = :user_from ORDER BY user_to DESC";

            $conn = $this->db->prepare($sql);
            $conn->bindValue(':user_from', $user_id, PDO::PARAM_STR);
        }
        else
        {
            $sql = "SELECT * FROM message ORDER BY id DESC";
            $conn = $this->db->prepare($sql);
        }

        try
        {
            $conn->execute();
            $result = $conn->fetchAll();
            return $result;
        } catch (PDOException $e)
        {
            echo 'Error reading message';
            echo $e->getMessage();
        }
    }
}


// $msg = new Message($db);

// // $msg->send('Okay, later', 2, 1, 0);
// // $msg->send('Okay!', 2, 1, 0);

// // $msgs = $msg->getThread(2, 1);
// $seen_time = date('Y:m:d H:i:s');

// $msg->seen(1, 1);

// $msgs = $msg->readMessage(1);

// $msgs = $msg->getMessages(1);

// var_dump($msgs);