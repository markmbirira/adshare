-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 02, 2018 at 07:31 AM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.5-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minitweet`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisement`
--

CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `votes` int(11) DEFAULT NULL,
  `file_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisement`
--

INSERT INTO `advertisement` (`id`, `title`, `path`, `description`, `category_id`, `user_id`, `created_time`, `status`, `votes`, `file_type`) VALUES
(1, 'How about a new title, yes', 'path/title/photo.png', '1st Post Here and there, edited, again and again', 1, 1, '2018-06-30 19:35:38', 0, 0, 'photo'),
(2, 'long title for advert - Noma!ll', 'path/title/photo.png', 'Second Post there, again.\r\n', 1, 1, '2018-07-01 10:27:55', 1, 0, 'photo'),
(3, 'title slang', 'path/title/photo.png', 'Something destroyed the original good feeling of the last shipper', 1, 1, '2018-06-30 19:41:12', 1, 0, 'photo'),
(4, 'title', 'path/title/photo.png', 'Who really cares??', 1, 1, '2018-06-29 20:23:33', 0, 0, 'photo'),
(5, 'edited again', 'path/title/photo.png', 'I can see it now...good job, finally updating', 1, 1, '2018-06-30 19:41:38', 1, 0, 'photo'),
(6, 'titler header', 'path/title/photo.png', 'Post description Here!', 1, 1, '2018-06-30 16:32:33', 1, 0, 'photo'),
(7, 'World Cup Bets!', '/uploads/worldcup-picks.jpg', 'This is the best of World Cup bets. Juicy J - Stop it', 1, 1, '2018-06-30 19:40:14', 1, 0, 'image/jpeg'),
(8, 'Sample picture', '/uploads/worldcup-picks.jpg', 'Some good event here, now!', 1, 1, '2018-06-27 08:07:34', 1, 0, 'image/jpeg'),
(9, 'Selling bed and mattress! Login today to access', '/uploads/pexels-photo-209841.jpeg', 'Hapa ni bed na matress. Ukitaka holler Mark at 0714034960', 3, 3, '2018-06-30 20:45:15', 1, 0, 'image/jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `advert_interest`
--

CREATE TABLE `advert_interest` (
  `id` int(11) NOT NULL,
  `advert_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time_liked` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `created_date`, `status`) VALUES
(1, 'electronics', 'Electronic products here. Buy one, buy all', '2018-06-29 18:42:27', 1),
(2, 'food', 'Electronic Products for second hand ownership of products', '2018-06-29 18:42:33', 1),
(3, 'clothing', 'Electronic Products for second hand ownership of products', '2018-06-29 18:42:36', 1),
(6, 'Notes', 'Notes for class', '2018-06-29 18:37:26', 0),
(7, 'shoes', 'All your best shoes. From sporting to swagging to church shoes. Come one come all', '2018-06-29 18:42:40', 1),
(8, 'Obanda', 'Things about Obanda listed here', '2018-06-29 17:44:14', 1),
(9, 'sufurias', 'cooking sufuria', '2018-06-30 16:42:16', 1),
(10, 'another1', 'Dj Khaleed Exlusively produced mixtapes and albums', '2018-07-01 05:48:33', 1),
(11, 'njeru', 'Laravel stuffee', '2018-07-01 08:28:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `timePosted` datetime NOT NULL,
  `author` varchar(100) NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

CREATE TABLE `feeds` (
  `id` int(11) NOT NULL,
  `body` varchar(100) NOT NULL,
  `timePosted` datetime NOT NULL,
  `status` smallint(6) NOT NULL,
  `author` varchar(100) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `votes` smallint(6) DEFAULT NULL,
  `down_votes` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feeds`
--

INSERT INTO `feeds` (`id`, `body`, `timePosted`, `status`, `author`, `path`, `type`, `votes`, `down_votes`) VALUES
(1, 'hello', '2018-06-22 16:52:33', 1, 'mark', '/uploads/29f31018-dc57-45f3-a96f-044c513a782a.png', 'image/png', NULL, NULL),
(2, 'hhr', '2018-06-22 17:37:14', 1, 'mark', '/uploads/Screenshot-2018-5-31 Free logo design(1).png', 'image/png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `message` text NOT NULL,
  `time_sent` datetime NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `seen_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `user_from`, `user_to`, `message`, `time_sent`, `seen`, `seen_time`) VALUES
(1, 2, 1, 'Hello, I saw your Ad today! Would like to chat...', '2018-06-24 21:10:16', 1, '2018-06-24 18:54:04'),
(2, 1, 2, 'Hello too, that will be greate. Holler up for the chat...', '2018-06-24 21:13:14', 0, NULL),
(3, 2, 1, 'Where should we chat? I dont have much time', '2018-06-24 21:13:57', 0, NULL),
(4, 1, 2, 'Anytime, from the evening especially. I am selling Computer monitors', '2018-06-24 21:14:40', 0, NULL),
(5, 2, 1, 'That\'s nice, will check them out, I need a bunch', '2018-06-24 21:15:15', 0, NULL),
(6, 1, 2, 'Okay, later', '2018-06-24 21:15:37', 0, NULL),
(7, 2, 1, 'Okay!', '2018-06-24 21:15:51', 0, NULL),
(8, 1, 2, 'hello there', '2018-06-24 23:30:09', 1, NULL),
(9, 1, 3, 'hello there', '2018-06-24 23:33:44', 1, NULL),
(10, 1, 2, 'Hello People', '2018-06-25 00:26:28', 1, NULL),
(11, 1, 3, 'hello man', '2018-06-25 08:30:42', 1, NULL),
(12, 1, 3, 'Good morning, I saw your product!', '2018-06-25 08:35:34', 1, NULL),
(13, 1, 2, 'hello there', '2018-06-25 10:08:09', 1, NULL),
(14, 1, 2, 'hello there 2', '2018-06-25 10:10:52', 1, NULL),
(15, 1, 2, 'bjjhb', '2018-06-25 10:17:28', 1, NULL),
(16, 1, 2, 'Hello there 3', '2018-06-25 10:18:44', 1, NULL),
(17, 1, 2, 'I am sending another one now!', '2018-06-25 10:19:58', 1, NULL),
(18, 1, 2, 'Hey, can we meet and talk!\n0714034960', '2018-06-25 10:20:36', 1, NULL),
(19, 1, 2, 'I sent you one message earlier!', '2018-06-25 10:22:02', 1, NULL),
(20, 1, 2, 'testing notifications ', '2018-06-25 10:41:13', 1, NULL),
(21, 1, 2, 'testing notifications ', '2018-06-25 10:43:31', 1, NULL),
(22, 1, 2, 'Hey buddy I got your details done!', '2018-06-25 10:52:00', 1, NULL),
(23, 1, 2, 'New message here, see it', '2018-06-25 13:19:42', 1, NULL),
(24, 1, 2, 'Another message', '2018-06-25 13:21:21', 1, NULL),
(25, 1, 2, 'ffkjfhr', '2018-06-27 09:08:27', 1, NULL),
(26, 1, 2, 'hey man, I saw your advert yesterday', '2018-07-01 08:34:49', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `notification_text` varchar(200) NOT NULL,
  `seen` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `user_to`, `notification_text`, `seen`) VALUES
(1, 2, 'You have a new message', 0),
(2, 1, 'You have a new message', 0),
(3, 2, 'You have a new message', 0),
(4, 2, 'You have a new message', 0),
(5, 2, 'You have a new message', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `memberSince` datetime NOT NULL,
  `userRole` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `path` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `memberSince`, `userRole`, `status`, `path`) VALUES
(1, 'test', 'Real', 'Person', 'test@mail.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '2018-06-24 18:48:18', 'user', 1, NULL),
(2, 'test1', 'Testercles', 'One', 'test1@mail.com', '1b4f0e9851971998e732078544c96b36c3d01cedf7caa332359d6f1d83567014', '2018-06-24 18:49:06', 'user', 1, NULL),
(3, 'Ad', 'Ad', 'Vert', 'advert@mail.com', 'a64463d76711f306bb55c0a28fa954bc39fae29844790228063775ee89c51448', '2018-06-24 19:11:06', 'user', 1, NULL),
(4, 'test2', 'Tester', 'Twotoo', 'test2@mail.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '2018-06-29 21:57:28', 'user', 1, NULL),
(5, 'sample', 'Sampler', 'User', 'sample@mail.com', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '2018-06-29 22:51:34', 'user', 1, NULL),
(6, 'mark', 'Mark', 'Mbirira', 'markmbirira@gmail.com', '090578b9e6d188277f97df669c08c660d27a4b84c243d237e450cfbae654930a', '2018-06-30 20:43:45', 'user', 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisement`
--
ALTER TABLE `advertisement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advert_interest`
--
ALTER TABLE `advert_interest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feeds`
--
ALTER TABLE `feeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisement`
--
ALTER TABLE `advertisement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `advert_interest`
--
ALTER TABLE `advert_interest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feeds`
--
ALTER TABLE `feeds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
